#!/usr/bin/python

import sys, getopt
from typing import List
from PIL import Image
import numpy
import re
from os import listdir
from os.path import isfile, join
import xlsxwriter
from datetime import datetime


def print_help():
    print(""" 
 _______ _________         _________ _______ 
(  ____ )\__   __/|\     /|\__   __/(  ____ \\
| (    )|   ) (   ( \   / )   ) (   | (    \/
| (____)|   | |    \ (_) /    | |   | (__    
|  _____)   | |     ) _ (     | |   |  __)   
| (         | |    / ( ) \    | |   | (      
| )      ___) (___( /   \ )___) (___| (____/\\ 
|/       \_______/|/     \|\_______/(_______/ 
                                             
                                             """)
    print('Pixel image analyzer for scientific purposes.')
    print('Counts pixels below given threshold. Threshold should be from 0 (black) to 255 (white):')
    print('Generate Excel report with area taken by matched pixels, along with processed images.\n')
    print('Usage: \n')
    print('One image preview: ')
    print('         python pixie.py -t 140 -k <len in km>,<len in km> <filename.tif>')
    print('         python pixie.py --threshold=140 --km=<len in km>,<len in km> <filename.tif>')
    print('Example: python pixie.py --threshold=140 --km=20,20 my_file.tif')

    print('\nMultiple images processing: ')
    print('         python pixie.py -t 140 -o <output_dir> -k <len in km>,<len in km> <directory_with_files>')
    print('         python pixie.py --threshold=140  --output=<output_dir> --km=<len in km>,<len in km> <directory_with_files>')
    print('Example: python pixie.py --threshold=140  --output=out --km=20,20 data_dir')


class RectangleImageData:
    def __init__(self, image_name: str, pixels: List[int], size: List[int], classified_pixels: int):
        self.image_name = image_name
        self.pixels = pixels
        self.km = size
        self.classified_pixels = classified_pixels
        self.whole_area_km = size[0] * size[1]
        pixel_area_in_sq_meters = (size[0] * 1000 * size[1] * 1000) / (pixels[0] * pixels[1])
        self.classified_area_meters = classified_pixels * pixel_area_in_sq_meters
        self.classified_area_km = self.classified_area_meters / (1000 * 1000)
        self.classified_area_pct = self.classified_area_km / self.whole_area_km


def test_image(filename, threshold, size_in_km: List[int]):
    img = Image.open(filename, 'r')
    imarray = numpy.array(img)
    imarray.shape

    print("Image size:", img.size)
    print("Processing. It may take a while....")
    counter = 0
    new_image = []

    for line in imarray:
        new_line = []
        for pixel in line:
            v = pixel[0]
            if v < threshold:
                counter += 1
                new_line.append([0, 0, 0])
            else:
                new_line.append([255, 255, 255])
        new_image.append(new_line)

    print("Pixels above threshold:", counter)
    Image.fromarray(numpy.array(new_image, dtype=numpy.uint8)).show()
    img.show()
    return RectangleImageData(image_name=filename, pixels=img.size, size=size_in_km, classified_pixels=counter)


def proces_image(filename, threshold, output, size_in_km: List[int]) -> RectangleImageData:
    img = Image.open(filename, 'r')
    imarray = numpy.array(img)
    imarray.shape
    print("Processing Image: " + filename)
    counter = 0
    new_image = []

    for line in imarray:
        new_line = []
        for pixel in line:
            # print(pixel)
            v = pixel[0]
            if v < threshold:
                counter += 1
                new_line.append([0, 0, 0])
            else:
                new_line.append([255, 255, 255])
        new_image.append(new_line)
    Image.fromarray(numpy.array(new_image, dtype=numpy.uint8)).save(output)
    return RectangleImageData(image_name=filename, pixels=img.size, size=size_in_km, classified_pixels=counter)


def main(argv):
    threshold = 0
    output = '.'
    len_in_km = [20, 20]
    try:
        opts, args = getopt.getopt(argv, "ho:t:k:", ["output=", "threshold=", "km="])
    except getopt.GetoptError:
        print('pixie.py [-o <outputdir>] -t <threshold> <inputfile>|<inputdir>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ("-t", "--threshold"):
            threshold = int(arg)
        elif opt in ("-o", "--output"):
            output = arg
        elif opt in ("-k", "--km"):
            len_in_km = list(map(lambda x: int(x), arg.split(",")))
    if len(opts) == 0:
        print_help()
        sys.exit()
    print('Provided pixel intensity Threshold is: ', threshold)
    print('Output directory is {}/'.format(output))
    filename = argv[-1]
    if re.search('tif', filename, re.IGNORECASE):
        image_data = test_image(filename, threshold, len_in_km)
        print("Image Data:")
        print("Image name", image_data.image_name)
        print("Image size in pixels", image_data.pixels)
        print("Image size in km", image_data.km)
        print("Image area in km^2", image_data.whole_area_km)
        print("No of classified pixels", image_data.classified_pixels)
        print("Classified area km", image_data.classified_area_km)
        print(" % of classified area", image_data.classified_area_pct)
    else:
        onlyfiles = [f for f in listdir(filename) if isfile(join(filename, f))]
        workbook = xlsxwriter.Workbook("Summary-{:%Y-%m-%d %H:%M}.xlsx".format(datetime.now()))
        worksheet = workbook.add_worksheet('Summary for threshold {}'.format(threshold))

        # cell formats
        bold = workbook.add_format({'bold': True, 'text_wrap': True})
        percentage_format = workbook.add_format({'num_format': '# %'})
        num_format = workbook.add_format({'num_format': '##'})
        num_format_float = workbook.add_format({'num_format': '#,##'})
        worksheet.set_column("A:A", 30)
        worksheet.set_column("B:B", 12)
        row = 0

        worksheet.write_row(row, 0, ["Image name", "Size in pixels", "Size [km]", "No. classified pixels",
                                           "Classified area [km^2]", "% of classified area"], bold)

        for file in onlyfiles:
            row += 1
            image_data = proces_image(filename + "/" + file, threshold, output + "/" + file, len_in_km)
            worksheet.write(row, 0, image_data.image_name.split("/")[-1])
            worksheet.write(row, 1, "{}x{}".format(image_data.pixels[0], image_data.pixels[1]))
            worksheet.write(row, 2, "{}x{}".format(image_data.km[0], image_data.km[1]))
            worksheet.write(row, 3, image_data.classified_pixels, num_format)
            worksheet.write(row, 4, image_data.classified_area_km, num_format_float)
            worksheet.write(row, 5, image_data.classified_area_pct, percentage_format)

        workbook.close()


if __name__ == "__main__":
    main(sys.argv[1:])
