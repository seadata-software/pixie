# Pixie
 ```
  _______ _________         _________ _______ 
(  ____ )\__   __/|\     /|\__   __/(  ____ \
| (    )|   ) (   ( \   / )   ) (   | (    \/
| (____)|   | |    \ (_) /    | |   | (__    
|  _____)   | |     ) _ (     | |   |  __)   
| (         | |    / ( ) \    | |   | (      
| )      ___) (___( /   \ )___) (___| (____/\ 
|/       \_______/|/     \|\_______/(_______/ 
                                             
                                           
Pixie is image analyzer for scientific purposes.
Counts pixels above below threshold in black and white pictures. Threshold should be from 0 (black) to 255 (white):
Generate Excel report with area taken by matched pixels, along with processed images showing clasification result.

Usage: 

One image preview: 
         python pixie.py -t 140 -k <len in km>,<len in km> <filename.tif>
         python pixie.py --threshold=140 --km=<len in km>,<len in km> <filename.tif>
Example: python pixie.py --threshold=140 --km=20,20 my_file.tif

Multiple images processing: 
         python pixie.py -t 140 -o <output_dir> -k <len in km>,<len in km> <directory_with_files>
         python pixie.py --threshold=140  --output=<output_dir> --km=<len in km>,<len in km> <directory_with_files>
Example: python pixie.py --threshold=140  --output=out --km=20,20 input_directory

```
 

 ## Requirements
 * [Python 3.6.5+](https://www.python.org/)

 ## Installation
 1. Clone or download zipped pixie repository. Unpack and go to pixie directory.
 2. Install python requirements executing:
 
    ```pip install -r requirements.txt``` 
 3. Show help executing:
    ```python pixie.py -h```

## Overview
Pixie is a python script.

Pixie automates simple image recognition.

Can analyze thousands of Black and White images. 

It's sole function is calculation of area covered by pixels of given intensity threshold.

Results are exported as xlsx file contained: 
- Image name    
- Image Size in pixels
- Image Area in km
- No. of classified pixels
- Classified area in km^2
- % of classified area

### How it works?
Pixie process every image one by one in given directory, and aggregates results in one xlsx report.

For each image pixie counts pixels above given intensity threshold in black and white pictures. Based on count of classfied pixels and whole area of the image pixie calculates the area covered by classified pixels and % of the whole area.

## Pixie contribution in scientific publications

 1. **[MDPI and ACS Style](https://www.mdpi.com/2073-4433/10/11/654)**
 
    Wenta, M.; Herman, A. Area-Averaged Surface Moisture Flux over Fragmented Sea Ice: Floe Size Distribution Effects and the Associated Convection Structure within the Atmospheric Boundary Layer.
    
    Atmosphere 2019, 10(11), 654; [https://doi.org/10.3390/atmos10110654](https://www.mdpi.com/2073-4433/10/11/654)
    
 2. **[MOST WIEDZY](https://mostwiedzy.pl/en/open-research-data/areas-of-updraft-air-motion-from-wrf-model-simulations,102042114114661-0) - Areas of updraft air motion from WRF model simulations.**
 
    Dataset is a part of numerical modelling study focusing on the analysis of sea ice floes size distribution (FSD) influence on the horizontal and vertical structure of convection in the atmosphere
    
    DOI: [10.34808/fwt2-bs21](https://mostwiedzy.pl/en/open-research-data/areas-of-updraft-air-motion-from-wrf-model-simulations,102042114114661-0)