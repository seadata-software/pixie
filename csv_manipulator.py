#!/usr/bin/python

import getopt
import sys
from os import listdir
from os.path import isfile, join
import csv


def print_help():
    print('Multiple CSV files manipulator for scientific purposes.')
    print('Usage: ')
    print(' python csv_manipulator.py [options] -o <out_directory> <input_directory>\n')
    print(' OPTIONS:')
    print(' -R n, --remove-column=n')
    print('         Removes nth column (starts with 0) from every row\n')
    print('Example: python csv_manipulator.py -RC 5 -o out csv_data_dir\n')


def main(argv):
    column_num = -1
    output_dir = "."
    try:
        opts, args = getopt.getopt(argv, "ho:R:", ["output=", "remove-column="])
    except getopt.GetoptError:
        print('csv_manipulator.py [-R] [-o <outputdir>] <inputdir>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ("-R", "--remove-column"):
            column_num = int(arg)
        elif opt in ("-o", "--output"):
            output_dir = arg
    if len(opts) == 0:
        print_help()
        sys.exit()
    print('Provided column: ', column_num)
    print('Output directory is {}/'.format(output_dir))
    input_dir = argv[-1]
    only_csv_files = [f for f in listdir(input_dir) if isfile(join(input_dir, f)) and ".csv" in f]

    # Processing Logic
    for file in only_csv_files:
        print("Processing: " + file + "\n")
        with open(input_dir + "/" + file, newline='') as csvfile, open(output_dir + "/" + file, 'w', newline='') as out_csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            csv_writer = csv.writer(out_csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for row in csv_reader:
                del row[column_num]
                csv_writer.writerow(row)


if __name__ == "__main__":
    main(sys.argv[1:])
